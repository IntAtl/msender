package ua.cn.msender.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "email_history")
public class EmailHistory implements Serializable {
	private static final long serialVersionUID = 4899566875037367919L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="code")
	private Integer code;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="uinfo", nullable=false)
	private UserInfo userInfo;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="worker", nullable=true)
	private Worker worker;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ugroup", nullable=true)
	private UserGroup userGroup;
	@Column(name="esubject")
	private String subject;
	@Column(name="eplot")
	private String plot;
	@Column(name="edate", columnDefinition="timestampt without time zone not null")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Worker getWorker() {
		return worker;
	}

	public void setWorker(Worker worker) {
		this.worker = worker;
	}

	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
}
