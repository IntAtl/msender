package ua.cn.msender.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "worker")
public class Worker implements Serializable {
	private static final long serialVersionUID = 4063994964197456521L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@Column(name = "full_name")
	private String fullName;
	@Column(name = "email")
	private String email;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_group", nullable = false)
	private UserGroup group = new UserGroup();
	@OneToMany(fetch=FetchType.LAZY, mappedBy="worker", cascade=CascadeType.ALL)
	private Set<EmailHistory> emails = new HashSet<EmailHistory>();

	public Worker() {
	}

	public Worker(String fullName, String email, String group) {
		this.fullName = fullName;
		this.email = email;
		this.group = new UserGroup(group);
	}

	public Worker(String fullName, String email, UserGroup group) {
		this.fullName = fullName;
		this.email = email;
		this.group = group;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserGroup getGroup() {
		return group;
	}

	public void setGroup(UserGroup group) {
		this.group = group;
	}

	public void setGroup(String group) {
		this.group = new UserGroup(group);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(Worker.class);
		str.append("[");
		str.append("code=").append(getCode()).append(", ");
		str.append("fullName=").append(getFullName()).append(", ");
		str.append("email=").append(getEmail()).append(", ");
		str.append("group=").append(getGroup().toString());
		str.append("]");
		return str.toString();
	}

	public Set<EmailHistory> getEmails() {
		return emails;
	}

	public void setEmails(Set<EmailHistory> emails) {
		this.emails = emails;
	}

}
