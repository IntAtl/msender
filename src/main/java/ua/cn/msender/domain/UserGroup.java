package ua.cn.msender.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "user_group")
public class UserGroup implements Serializable {
	private static final long serialVersionUID = -6485363441971098020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@Column(name = "group_name")
	private String name;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "group", cascade=CascadeType.ALL)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE)
	private Set<UserInfo> users = new HashSet<UserInfo>();
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "group", cascade=CascadeType.ALL)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE)
	private Set<Worker> workers = new HashSet<Worker>();
	@OneToMany(fetch=FetchType.LAZY, mappedBy="userGroup", cascade=CascadeType.ALL)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE)
	private Set<EmailHistory> emails = new HashSet<EmailHistory>();

	public UserGroup() {
	}

	public UserGroup(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserInfo> getUsers() {
		return users;
	}

	public void setUsers(Set<UserInfo> users) {
		this.users = users;
	}

	public Set<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(Set<Worker> workers) {
		this.workers = workers;
	}

	@Override
	public String toString() {
		return "[code=" + getCode() + ", name=" + getName() + "]";
	}

	public Set<EmailHistory> getEmails() {
		return emails;
	}

	public void setEmails(Set<EmailHistory> emails) {
		this.emails = emails;
	}

}
