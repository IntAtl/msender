package ua.cn.msender.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_info")
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1759258889030408880L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@Column(name = "user_login")
	private String login;
	@Column(name = "user_email")
	private String email;
	@Column(name = "user_password")
	private String password;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_group", nullable=false)
	private UserGroup group;
	@OneToMany(fetch=FetchType.LAZY, mappedBy="userInfo", cascade=CascadeType.ALL)
	private Set<EmailHistory> emails = new HashSet<EmailHistory>();

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserGroup getGroup() {
		return group;
	}

	public void setGroup(UserGroup group) {
		this.group = group;
	}

	public Set<EmailHistory> getEmails() {
		return emails;
	}

	public void setEmails(Set<EmailHistory> emails) {
		this.emails = emails;
	}

}
