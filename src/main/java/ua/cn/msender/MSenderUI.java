package ua.cn.msender;
import javax.servlet.annotation.WebServlet;

import ua.cn.msender.form.FormFactory;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

@Theme("msender")
public class MSenderUI extends UI {

	private static final long serialVersionUID = 1L;
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MSenderUI.class, widgetset="ua.cn.msender.widgetset.MSenderLoginFormWidgetset")
	public static class Servlet extends VaadinServlet {
		private static final long serialVersionUID = 1L;
	}
	@Override
	protected void init(VaadinRequest request) {
		if(UI.getCurrent().getSession().getAttribute(VaadinSessionAttribute.USER_ID) == null) {
			UI.getCurrent().setContent(FormFactory.createLoginForm());
		} else {
			UI.getCurrent().setContent(FormFactory.createMainForm());
		}
	}

	public void setSessionAttribute(String key, Object value) {
		try {
			VaadinSession.getCurrent().getLockInstance().lock();
			VaadinSession.getCurrent().setAttribute(key, value);
		} finally {
			VaadinSession.getCurrent().getLockInstance().unlock();
		}
	}
}
