package ua.cn.msender.component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import ua.cn.msender.domain.EmailHistory;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.UserInfo;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Table;

public class EmailHistoryTable extends Table {
	private static final long serialVersionUID = 599125227043281984L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private static String USER_INFO_VISIBLE_COLUMN = "userInfo";
	private static String WORKER_VISIBLE_COLUMN = "worker";
	private static String USER_GROUP_VISIBLE_COLUMN = "userGroup";
	private static String SUBJECT_VISIBLE_COLUMN = "subject";
	private static String PLOT_VISIBLE_COLUMN = "plot";
	private static String DATE_VISIBLE_COLUMN = "sendDate";

	public EmailHistoryTable() {
		super();
		BeanItemContainer<EmailHistory> cont = new BeanItemContainer<EmailHistory>(
				EmailHistory.class);
		this.setContainerDataSource(cont);
		this.setVisibleColumns(getTableVisibleColumns());
		this.setColumnHeaderMode(ColumnHeaderMode.EXPLICIT);
		this.setColumnHeaders(getTableColumnHeaders());
	}

	@Override
	public void attach() {
		super.attach();
	}

	@Override
	protected String formatPropertyValue(Object rowId, Object colId,
			Property<?> property) {
		if (property.getValue() instanceof UserInfo) {
			UserInfo ug = (UserInfo) property.getValue();
			return ug.getEmail();
		}
		if (property.getValue() instanceof Worker) {
			Worker ug = (Worker) property.getValue();
			return ug.getFullName();
		}
		if (property.getValue() instanceof UserGroup) {
			UserGroup ug = (UserGroup) property.getValue();
			return ug.getName();
		}
		if (property.getValue() instanceof Date) {
			Date dateValue = (Date) property.getValue();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
			return sdf.format(dateValue);
		}
		return super.formatPropertyValue(rowId, colId, property);
	}

	private String[] getTableColumnHeaders() {
		return new String[] {
				BUNDLE.getString("email.history.table.header.from"),
				BUNDLE.getString("email.history.table.header.to.worker"),
				BUNDLE.getString("email.history.table.header.to.group"),
				BUNDLE.getString("email.history.table.header.subject"),
				BUNDLE.getString("email.history.table.header.plot"),
				BUNDLE.getString("email.history.table.header.date") };
	}

	private Object[] getTableVisibleColumns() {
		return new Object[] { USER_INFO_VISIBLE_COLUMN, WORKER_VISIBLE_COLUMN,
				USER_GROUP_VISIBLE_COLUMN, SUBJECT_VISIBLE_COLUMN,
				PLOT_VISIBLE_COLUMN, DATE_VISIBLE_COLUMN };
	}
}
