package ua.cn.msender.component;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.WorkerDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

public class UsersTable extends Table {
	private static final long serialVersionUID = 5456508726702753062L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private static final String FULL_NAME_COLUMN = "fullName";
	private static final String USER_EMAIL_COLUMN = "email";
	private static final String USER_GROUP_COLUMN = "group";
	private static final Object USER_ACTION_COLUMN = "Action";

	public UsersTable() {
		super();
		BeanItemContainer<Worker> cont = new BeanItemContainer<Worker>(
				Worker.class);
		this.setContainerDataSource(cont);
		this.addGeneratedColumn("Action", new ColumnGenerator() {
			private static final long serialVersionUID = 8204477832297342783L;

			@Override
			public Object generateCell(Table source, final Object itemId,
					Object columnId) {
				Layout layout = new HorizontalLayout();
				Button sendEmail = new Button();
				sendEmail.setData(itemId);
				sendEmail.setStyleName(BaseTheme.BUTTON_LINK);
				sendEmail.setIcon(new ThemeResource("icons/email.png"));
				sendEmail.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 2601693938234563699L;

					@Override
					public void buttonClick(ClickEvent event) {
						Worker worker = (Worker)itemId;
						SendEmailWindow w = ComponentFactory.createSendEmailToWorkerEmailWindow(worker);
						UI.getCurrent().addWindow(w);
						w.center();
						
					}
				});
				Button delete = new Button();
				delete.setData(itemId);
				delete.setImmediate(true);
				delete.setStyleName(BaseTheme.BUTTON_LINK);
				delete.setIcon(new ThemeResource("icons/delete.gif"));
				delete.addClickListener(new ClickListener() {
					private static final long serialVersionUID = -8123154542123840527L;

					@Override
					public void buttonClick(ClickEvent event) {
						WorkerDAO.deleteObject(itemId);
						@SuppressWarnings("unchecked")
						BeanItemContainer<Worker> cont = (BeanItemContainer<Worker>) getContainerDataSource();
						cont.removeItem(itemId);
						refreshRowCache();
					}
				});

				Button edit = new Button();
				edit.setData(itemId);
				edit.setImmediate(true);
				edit.setStyleName(BaseTheme.BUTTON_LINK);
				edit.setIcon(new ThemeResource("icons/edit.png"));
				edit.addClickListener(new ClickListener() {
					private static final long serialVersionUID = -142473677411826057L;

					@Override
					public void buttonClick(ClickEvent event) {
						@SuppressWarnings("unchecked")
						BeanItemContainer<Worker> container = (BeanItemContainer<Worker>) UsersTable.this
								.getContainerDataSource();
						AddUserWindow w = ComponentFactory
								.createAddWorkerWindow(container);
						w.getAddButton().setVisible(false);
						w.getEditButton().setVisible(true);
						w.getFieldGroup().setItemDataSource((Worker) itemId);
						w.setWorkersTable(UsersTable.this);
						UI.getCurrent().addWindow(w);
						w.center();
					}
				});

				layout.addComponent(sendEmail);
				layout.addComponent(edit);
				layout.addComponent(delete);
				return layout;
			}
		});
		this.setVisibleColumns(getTableVisibleColumns());
		this.setColumnHeaderMode(ColumnHeaderMode.EXPLICIT);
		this.setColumnHeaders(getTableColumnHeaders());
	}

	public String[] getTableColumnHeaders() {
		return new String[] {
				BUNDLE.getString("bottom.form.users.managment.tab.users.table.header.fullname"),
				BUNDLE.getString("bottom.form.users.managment.tab.users.table.header.email"),
				BUNDLE.getString("bottom.form.users.managment.tab.users.table.header.group"),
				BUNDLE.getString("bottom.form.users.managment.tab.users.table.header.action") };
	}

	public Object[] getTableVisibleColumns() {
		return new Object[] { FULL_NAME_COLUMN, USER_EMAIL_COLUMN,
				USER_GROUP_COLUMN, USER_ACTION_COLUMN };
	}

	@Override
	protected String formatPropertyValue(Object rowId, Object colId,
			Property<?> property) {
		if (property.getValue() instanceof UserGroup) {
			UserGroup ug = (UserGroup) property.getValue();
			return ug.getName();
		}
		return super.formatPropertyValue(rowId, colId, property);
	}
}
