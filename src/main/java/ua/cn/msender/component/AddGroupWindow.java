package ua.cn.msender.component;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.form.AddGroupForm;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

public class AddGroupWindow extends Window {
	private static final long serialVersionUID = 4421814737041979788L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private AddGroupForm groupForm;
	private Button okBtn;
	private Button cancelBtn;
	private Button editBtn;
	private BeanFieldGroup<UserGroup> fieldGroup;

	public AddGroupWindow() {
		setCaption(BUNDLE.getString("add.group.form.caption"));
	}

	@Override
	public void attach() {
		super.attach();
		VerticalLayout vl = new VerticalLayout();
		vl.addComponent(getGroupForm());

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(getOkBtn());
		hl.addComponent(getEditBtn());
		hl.addComponent(getCancelBtn());

		vl.addComponent(hl);
		getFieldGroup();
		setContent(vl);
	}

	public AddGroupForm getGroupForm() {
		if (this.groupForm == null) {
			this.groupForm = new AddGroupForm();
		}
		return groupForm;
	}

	public Button getOkBtn() {
		if (this.okBtn == null) {
			this.okBtn = new Button();
			this.okBtn.setCaption(BUNDLE.getString("add.button"));
			this.okBtn.setImmediate(true);
			this.okBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 8763079403950523436L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						if (getFieldGroup().isValid()) {
							getFieldGroup().commit();
							BeanItem<UserGroup> item = (BeanItem<UserGroup>) getFieldGroup()
									.getItemDataSource();
							UserGroup group = item.getBean();
							GroupDAO.saveGroup(group);
							close();
						} else {
							Notification.show(
									BUNDLE.getString("add.group.form.error"),
									Type.ERROR_MESSAGE);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		return okBtn;
	}

	public Button getCancelBtn() {
		if (this.cancelBtn == null) {
			this.cancelBtn = new Button();
			this.cancelBtn.setCaption(BUNDLE.getString("cancel.button"));
			this.cancelBtn.setImmediate(true);
			this.cancelBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -7344721806292555638L;

				@Override
				public void buttonClick(ClickEvent event) {
					getFieldGroup().discard();
					close();
				}
			});
		}
		return cancelBtn;
	}

	public Button getEditBtn() {
		if (this.editBtn == null) {
			this.editBtn = new Button();
			this.editBtn.setCaption(BUNDLE.getString("edit.button"));
			this.editBtn.setImmediate(true);
		}
		return editBtn;
	}

	public BeanFieldGroup<UserGroup> getFieldGroup() {
		if (this.fieldGroup == null) {
			this.fieldGroup = new BeanFieldGroup<UserGroup>(UserGroup.class);
			this.fieldGroup.setItemDataSource(new BeanItem<UserGroup>(
					new UserGroup()));
			this.fieldGroup.bind(getGroupForm().getName(), "name");
			this.fieldGroup.setBuffered(true);
		}
		return fieldGroup;
	}
}
