package ua.cn.msender.component;

import ua.cn.msender.domain.Worker;

import com.vaadin.data.util.BeanItemContainer;

public class ComponentFactory {
	public static AddUserWindow createAddWorkerWindow(BeanItemContainer<Worker> container) {
		return new AddUserWindow(container);
	}
	
	public static AddGroupWindow createAddGroupWindow() {
		AddGroupWindow w = new AddGroupWindow();
		w.setClosable(true);
		w.setImmediate(true);
		w.setResizable(false);
		return w;
	}
	
	public static SendEmailWindow createSendEmailToWorkerEmailWindow(Worker worker) {
		SendEmailWindow w = new SendEmailWindow(worker);
		w.setClosable(true);
		w.setImmediate(true);
		w.setResizable(false);
		w.getSendToWorkerButton().setVisible(true);
		w.getSendToGroupButton().setVisible(false);
		w.getSendEmailForm().getGroupsCmb().setVisible(false);
		
		return w;
	}
	
	public static SendEmailWindow createSendEmailToGroupEmailWindow() {
		SendEmailWindow w = new SendEmailWindow();
		w.setClosable(true);
		w.setImmediate(true);
		w.setResizable(false);
		w.getSendToWorkerButton().setVisible(false);
		w.getSendToGroupButton().setVisible(true);
		w.getSendEmailForm().getGroupsCmb().setVisible(true);
		return w;
	}
}
