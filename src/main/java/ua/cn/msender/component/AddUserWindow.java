package ua.cn.msender.component;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.dao.hibernate.WorkerDAO;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.form.AddUserInfoForm;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

public class AddUserWindow extends Window {
	private static final long serialVersionUID = -5146101140194912243L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private Button editButton = null;
	private Button addButton = null;
	private Button cancelButton = null;
	private AddUserInfoForm userForm = null;
	private BeanFieldGroup<Worker> fieldGroup = null;
	private BeanItemContainer<Worker> workersContainer;
	private UsersTable table;

	public AddUserWindow(BeanItemContainer<Worker> workersContainer) {
		super();
		this.workersContainer = workersContainer;
		setResizable(false);
		setClosable(true);
		setImmediate(true);
	}

	@Override
	public void attach() {
		super.attach();
		Layout ml = new VerticalLayout();
		ml.addComponent(getUserForm());
		Layout bl = new HorizontalLayout();
		bl.addComponent(getAddButton());
		bl.addComponent(getEditButton());
		bl.addComponent(getCancelButton());
		ml.addComponent(bl);
		getFieldGroup();
		setContent(ml);
	}

	public Button getEditButton() {
		if (this.editButton == null) {
			this.editButton = new Button();
			this.editButton.setCaption(BUNDLE.getString("edit.button"));
			this.editButton.setImmediate(true);
			this.editButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -7032844339107035913L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						if(getFieldGroup().isValid()) {
							getFieldGroup().commit();
							table.refreshRowCache();
							
							BeanItem<Worker> item = getFieldGroup().getItemDataSource();
							Worker bean = (Worker) (item.getBean() == null ? new Exception() : item.getBean());
							System.out.println(bean.toString());
							WorkerDAO.updateObject(bean);
							close();
						} else {
							Notification.show(BUNDLE.getString("add.worker.form.error"), Type.ERROR_MESSAGE);
						}
					} catch (Exception e) {
						getFieldGroup().discard();
						e.printStackTrace();
					}
				}
			});
		}
		return editButton;
	}

	public Button getAddButton() {
		if (this.addButton == null) {
			this.addButton = new Button();
			this.addButton.setCaption(BUNDLE.getString("add.button"));
			this.addButton.setImmediate(true);
			this.addButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -7032844339107035913L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						if (getFieldGroup().isValid()) {
							getFieldGroup().commit();
							BeanItem<Worker> item = (BeanItem<Worker>) getFieldGroup()
									.getItemDataSource();
							Worker w = item.getBean();
							w.setGroup(GroupDAO.getUserGroupByName(w.getGroup()
									.getName()));
							WorkerDAO.saveObject(w);
							getWorkersContainer().addBean(w);
							close();
						} else {
							Notification.show(
									BUNDLE.getString("add.worker.form.error"),
									Type.ERROR_MESSAGE);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		return addButton;
	}

	public Button getCancelButton() {
		if (this.cancelButton == null) {
			this.cancelButton = new Button();
			this.cancelButton.setCaption(BUNDLE.getString("cancel.button"));
			this.cancelButton.setImmediate(true);
			this.cancelButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});
		}
		return cancelButton;
	}

	public AddUserInfoForm getUserForm() {
		if (this.userForm == null) {
			this.userForm = new AddUserInfoForm();
		}
		return this.userForm;
	}

	public BeanFieldGroup<Worker> getFieldGroup() {
		if (this.fieldGroup == null) {
			this.fieldGroup = new BeanFieldGroup<Worker>(Worker.class);
			this.fieldGroup
					.setItemDataSource(new BeanItem<ua.cn.msender.domain.Worker>(
							new ua.cn.msender.domain.Worker("", "", "")));
			this.fieldGroup.bind(getUserForm().getFullName(), "fullName");
			this.fieldGroup.bind(getUserForm().getEmail(), "email");
			this.fieldGroup.bind(getUserForm().getGroup(), "group");
			this.fieldGroup.setBuffered(true);
		}
		return this.fieldGroup;
	}

	public BeanItemContainer<Worker> getWorkersContainer() {
		return this.workersContainer;
	}

	public void setWorkersTable(UsersTable usersTable) {
		this.table = usersTable;
	}
}
