package ua.cn.msender.component;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

public class GroupTable extends Table {
	private static final long serialVersionUID = 7365777410404050105L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private static final String GROUP_NAME_VISIBLE_COLUMN = "name";
	private static final String ACTION_VISIBLE_COLUMN = "action";

	public GroupTable() {
		super();
		BeanItemContainer<UserGroup> container = new BeanItemContainer<UserGroup>(
				UserGroup.class);
		this.setContainerDataSource(container);
		this.addGeneratedColumn(ACTION_VISIBLE_COLUMN, new ColumnGenerator() {
			private static final long serialVersionUID = 2367497747429885820L;

			@Override
			public Object generateCell(Table source, final Object itemId,
					Object columnId) {
				HorizontalLayout hLayout = new HorizontalLayout();

				Button delete = new Button();
				delete.setData(itemId);
				delete.setImmediate(true);
				delete.setStyleName(BaseTheme.BUTTON_LINK);
				delete.setIcon(new ThemeResource("icons/delete.gif"));
				delete.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 583266892282168229L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							UserGroup ugGroup = (UserGroup) itemId;
							GroupDAO.deleteGroup(ugGroup.getCode());
							getContainerDataSource().removeItem(itemId);
							refreshRowCache();
							Notification.show(BUNDLE.getString("group.table.delete.success"));
						} catch (Exception exception) {
							Notification.show("Error", Type.ERROR_MESSAGE);
							exception.printStackTrace();
						}
					}
				});

				hLayout.addComponent(delete);
				return hLayout;
			}
		});
		this.setVisibleColumns(getTableVisibleColumns());
		this.setColumnHeaderMode(ColumnHeaderMode.EXPLICIT);
		this.setColumnHeaders(getTableColumnHeaders());
	}

	private String[] getTableColumnHeaders() {
		return new String[] { BUNDLE.getString("group.table.caption.name"),
				BUNDLE.getString("group.table.caption.action") };
	}

	private Object[] getTableVisibleColumns() {
		// TODO Auto-generated method stub
		return new Object[] { GROUP_NAME_VISIBLE_COLUMN, ACTION_VISIBLE_COLUMN };
	}

	@Override
	public void attach() {
		super.attach();
	}

}
