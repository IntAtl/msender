package ua.cn.msender.component;

import java.util.Date;
import java.util.ResourceBundle;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

import ua.cn.msender.VaadinSessionAttribute;
import ua.cn.msender.dao.hibernate.EmailHistoryDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.UserInfo;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.form.SendEmailForm;
import ua.cn.msender.util.CaptionBundle;
import ua.cn.msender.util.EmailUtil;

public class SendEmailWindow extends Window {
	private static final long serialVersionUID = -4384352556766092362L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();

	private SendEmailForm sendEmailForm;
	private Button sendToWorkerButton;
	private Button sendToGroupButton;
	private Button cancelButton;
	private Worker worker;

	public SendEmailWindow(Worker worker) {
		this();
		this.worker = worker;
	}

	public SendEmailWindow() {
		super();
		setCaption(BUNDLE.getString("send.email.form.caption"));
	}

	@Override
	public void attach() {
		super.attach();
		VerticalLayout vl = new VerticalLayout();
		vl.addComponent(getSendEmailForm());

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(getSendToWorkerButton());
		hl.addComponent(getSendToGroupButton());
		hl.addComponent(getCancelButton());

		vl.addComponent(hl);

		setContent(vl);
	}

	public SendEmailForm getSendEmailForm() {
		if (this.sendEmailForm == null) {
			this.sendEmailForm = new SendEmailForm();
		}
		return this.sendEmailForm;
	}

	public Button getSendToWorkerButton() {
		if (this.sendToWorkerButton == null) {
			this.sendToWorkerButton = new Button();
			this.sendToWorkerButton.setCaption(BUNDLE
					.getString("send.email.form.send"));
			this.sendToWorkerButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 3174466258232231653L;

				@Override
				public void buttonClick(ClickEvent event) {
					if (getSendEmailForm().getSubjectField().isValid()) {
						String subject = getSendEmailForm().getSubjectField()
								.getValue();
						String plot = getSendEmailForm().getPlotField()
								.getValue();
						EmailUtil.sendEmailToWorker(worker.getEmail(), subject,
								plot);

						Integer userId = (Integer) UI.getCurrent().getSession()
								.getAttribute(VaadinSessionAttribute.USER_ID);
						UserInfo userInfo = new UserInfo();
						userInfo.setCode(userId);
						EmailHistoryDAO.saveObject(userInfo, worker, null,
								subject, plot, new Date());
						close();
						Notification.show(BUNDLE
								.getString("send.email.form.worker.success"));
					} else {
						Notification.show(
								BUNDLE.getString("send.email.form.error"),
								Type.ERROR_MESSAGE);
					}
				}
			});
		}
		return sendToWorkerButton;
	}

	public Button getSendToGroupButton() {
		if (this.sendToGroupButton == null) {
			this.sendToGroupButton = new Button();
			this.sendToGroupButton.setCaption(BUNDLE
					.getString("send.email.form.send"));
			this.sendToGroupButton.setImmediate(true);
			this.sendToGroupButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -7301311718801073536L;

				@Override
				public void buttonClick(ClickEvent event) {
					if (getSendEmailForm().getSubjectField().isValid()) {
						UserGroup uGroup = (UserGroup) getSendEmailForm()
								.getGroupsCmb().getValue();
						String subject = getSendEmailForm().getSubjectField()
								.getValue();
						String plot = getSendEmailForm().getPlotField()
								.getValue();
						EmailUtil.sendEmailToGroup(uGroup, subject, plot);
						Integer userId = (Integer) UI.getCurrent().getSession()
								.getAttribute(VaadinSessionAttribute.USER_ID);
						UserInfo userInfo = new UserInfo();
						userInfo.setCode(userId);
						EmailHistoryDAO.saveObject(userInfo, null, uGroup,
								subject, plot, new Date());
						close();
						Notification.show(BUNDLE
								.getString("send.email.form.group.success"));
					} else {
						Notification.show(
								BUNDLE.getString("send.email.form.error"),
								Type.ERROR_MESSAGE);
					}
				}
			});

		}
		return this.sendToGroupButton;
	}

	public Button getCancelButton() {
		if (this.cancelButton == null) {
			this.cancelButton = new Button();
			this.cancelButton.setCaption(BUNDLE
					.getString("send.email.form.cancel"));
			this.cancelButton.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 3174466258232231653L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});
		}
		return cancelButton;
	}

}
