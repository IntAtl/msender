package ua.cn.msender.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.HibernateUtil;

public class WorkerDAO {
	public static List<Worker> getWorkers() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from Worker");
		@SuppressWarnings("unchecked")
		List<Worker> list = query.list();
		return list.size() == 0 ? null : list;
	}

	public static Worker getWorkerByEmail(final String email) {
		if (email == null || email.isEmpty())
			return null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session
				.createQuery("from Worker w where w.email = :email");
		query.setParameter("email", email);
		@SuppressWarnings("unchecked")
		List<Worker> list = query.list();
		return list.size() == 0 ? null : list.get(0);
	}

	public static void deleteObject(Object o) {
		Session sess = HibernateUtil.getSessionFactory().getCurrentSession();
		sess.delete(o);
	}

	public static void saveObject(Worker worker) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.save(worker);
		session.flush();
	}

	public static void updateObject(Worker worker) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.update(worker);
		session.flush();
	}

	public static List<Worker> getWorkersByGroup(UserGroup group) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session
				.createQuery("from Worker w where w.group.name = :name");
		query.setParameter("name", group.getName());
		@SuppressWarnings("unchecked")
		List<Worker> list = query.list();
		return list.size() == 0 ? null : list;
	}
	
	public static Worker loadWorker(Integer idInteger) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Worker)session.load(Worker.class, idInteger);
	}
	
	public static void changeGroup(UserGroup ugGroup, Integer idInteger) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Worker worker = (Worker) session.get(Worker.class, idInteger);
		worker.setGroup(ugGroup);
		session.saveOrUpdate(worker);
	}
}
