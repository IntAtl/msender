package ua.cn.msender.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ua.cn.msender.domain.UserInfo;
import ua.cn.msender.util.HibernateUtil;

public class UserInfoDAO {
	public static UserInfo getUserInfoByLoginAndPassword(String login,
			String password) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session
				.createQuery("from UserInfo where login = :login and password = :pass");
		query.setParameter("login", login);
		query.setParameter("pass", password);
		@SuppressWarnings("unchecked")
		List<UserInfo> list = query.list();
		return list.size() == 0 ? null : list.get(0);
	}

	public static List<UserInfo> getUsers() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from UserInfo");
		@SuppressWarnings("unchecked")
		List<UserInfo> list = query.list();
		return list.size() == 0 ? null : list;
	}

	public static UserInfo getuserInfoById(Integer userId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from UserInfo ui where ui.code = :code");
		query.setParameter("code", userId);
		@SuppressWarnings("unchecked")
		List<UserInfo> list = query.list();
		return list.size() == 0 ? null : list.get(0);
	}
	
	public static UserInfo loadUserInfoById(Integer id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		return (UserInfo)session.load(UserInfo.class, id);
	}
}
