package ua.cn.msender.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.util.HibernateUtil;

public class GroupDAO {

	public static List<String> getGroups() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("select ug.name from UserGroup ug");
		@SuppressWarnings("unchecked")
		List<String> list = query.list();
		return list.size() == 0 ? null : list;
	}
	
	public static List<UserGroup> getGroupsEnt() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from UserGroup");
		@SuppressWarnings("unchecked")
		List<UserGroup> list = query.list();
		return list.size() == 0 ? null : list;
	}
	
	public static Integer saveGroup(UserGroup group) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		Integer id = (Integer) session.save(group);
		session.flush();
		return id;
	}
	
	public static UserGroup getUserGroupByName(String name) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from UserGroup where name = :name");
		query.setParameter("name", name);
		@SuppressWarnings("unchecked")
		List<UserGroup> list = query.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	public static UserGroup loadUserGroup(Integer idInteger) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		return (UserGroup)session.load(UserGroup.class, idInteger);
	}
	
	public static void deleteGroup(Integer idInteger) throws Exception{
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		UserGroup ugGroup = (UserGroup) session.get(UserGroup.class, idInteger);
		session.delete(ugGroup);
		session.getTransaction().commit();
	}
}
