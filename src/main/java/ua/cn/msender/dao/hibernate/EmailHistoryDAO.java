package ua.cn.msender.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ua.cn.msender.domain.EmailHistory;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.UserInfo;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.HibernateUtil;

public class EmailHistoryDAO {
	public static Integer saveObject(EmailHistory emailHistory) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Integer idInteger = (Integer) session.save(emailHistory);
		session.flush();
		return idInteger;
	}

	public static Integer saveObject(UserInfo userInfo, Worker worker,
			UserGroup userGroup, String subject, String plot, Date date) {
		UserInfo ui = UserInfoDAO.loadUserInfoById(userInfo.getCode());
		Worker w = null;
		if (worker != null && worker.getCode() != null)
			w = WorkerDAO.loadWorker(worker.getCode());
		UserGroup uGroup = null;
		if (userGroup != null && userGroup.getCode() != null)
			uGroup = GroupDAO.loadUserGroup(userGroup.getCode());
		EmailHistory eh = new EmailHistory();
		eh.setUserInfo(ui);
		eh.setWorker(w);
		eh.setUserGroup(uGroup);
		eh.setSubject(subject);
		eh.setPlot(plot);
		eh.setSendDate(date);
		
		return saveObject(eh);
	}
	
	public static List<EmailHistory> getEmailHistory() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("from EmailHistory");
		@SuppressWarnings("unchecked")
		List<EmailHistory> list = query.list();
		return list;
	}
}
