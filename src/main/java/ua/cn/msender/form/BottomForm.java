package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.EmailHistoryDAO;
import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.dao.hibernate.WorkerDAO;
import ua.cn.msender.domain.EmailHistory;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;

public class BottomForm extends GridLayout {
	private static final long serialVersionUID = 8925833990926646952L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	
	private TabSheet tabSheet;
	private UsersManagmentTab userManagmentTab;
	private EmailHistoryTab emailHistoryTab;
	private GroupTab groupTab;
	
	@Override
	public void attach() {
		super.attach();
		addComponent(getTabSheet(), 0, 0);
		setComponentAlignment(getTabSheet(), Alignment.MIDDLE_CENTER);
	}

	public BottomForm() {
		setColumns(1);
		setRows(1);
		setSizeFull();
	}
	
	public TabSheet getTabSheet() {
		if(this.tabSheet == null) {
			this.tabSheet = new TabSheet();
			this.tabSheet.addTab(getUsersManagmentTab(), BUNDLE.getString("bottom.form.users.managment.tab.caption.users.managment"));
			this.tabSheet.addTab(getGroupTab(), BUNDLE.getString("bottom.form.groups.tab.caption"));
			this.tabSheet.addTab(getEmailHistoryTab(), BUNDLE.getString("bottom.form.users.managment.tab.caption.email.history"));
			this.tabSheet.addSelectedTabChangeListener(new SelectedTabChangeListener() {
				private static final long serialVersionUID = 6675741471176910L;

				@Override
				public void selectedTabChange(SelectedTabChangeEvent event) {
					TabSheet tabSheet = event.getTabSheet();
					Component component = tabSheet.getSelectedTab();
					if (component instanceof UsersManagmentTab) {
						@SuppressWarnings("unchecked")
						BeanItemContainer<Worker> cont = (BeanItemContainer<Worker>) getUsersManagmentTab().getUsersTable().getContainerDataSource();
						cont.removeAllItems();
						cont.addAll(WorkerDAO.getWorkers());
					}
					if (component instanceof GroupTab) {
						@SuppressWarnings("unchecked")
						BeanItemContainer<UserGroup> gContainer = (BeanItemContainer<UserGroup>) getGroupTab().getGroupTable().getContainerDataSource();
						gContainer.removeAllItems();
						gContainer.addAll(GroupDAO.getGroupsEnt());
					}
					if (component instanceof EmailHistoryTab) {
						@SuppressWarnings("unchecked")
						BeanItemContainer<EmailHistory> eContainer = (BeanItemContainer<EmailHistory>) getEmailHistoryTab().getEmailHistoryTable().getContainerDataSource();
						eContainer.removeAllItems();
						eContainer.addAll(EmailHistoryDAO.getEmailHistory());
					}
				}
			});
		}
		return this.tabSheet;
	}
	
	public UsersManagmentTab getUsersManagmentTab() {
		if(this.userManagmentTab == null) {
			this.userManagmentTab = new UsersManagmentTab();
		}
		return this.userManagmentTab;
	}

	public EmailHistoryTab getEmailHistoryTab() {
		if(this.emailHistoryTab == null) {
			this.emailHistoryTab = new EmailHistoryTab();
		}
		return emailHistoryTab;
	}

	public GroupTab getGroupTab() {
		if(this.groupTab == null) {
			this.groupTab = new GroupTab();
		}
		return groupTab;
	}
}
