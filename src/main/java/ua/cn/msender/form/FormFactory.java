package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.MSenderUI;
import ua.cn.msender.VaadinSessionAttribute;
import ua.cn.msender.dao.hibernate.UserInfoDAO;
import ua.cn.msender.domain.UserInfo;
import ua.cn.msender.util.CaptionBundle;

import com.ejt.vaadin.loginform.DefaultVerticalLoginForm;
import com.ejt.vaadin.loginform.LoginForm;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class FormFactory {
	private static ResourceBundle BUNDLE = CaptionBundle.getBundle();
	public static LoginForm createLoginForm() {
		return new DefaultVerticalLoginForm() {
			private static final long serialVersionUID = 2919607444584767512L;

			@Override
			protected void login(String userName, String password) {
				UserInfo user = UserInfoDAO.getUserInfoByLoginAndPassword(userName, password);
				if (user != null) {
					MSenderUI ui = (MSenderUI)UI.getCurrent();
					ui.setSessionAttribute(VaadinSessionAttribute.USER_ID, user.getCode());
					ui.setSessionAttribute(VaadinSessionAttribute.LOGIN, user.getLogin());
					ui.setContent(FormFactory.createMainForm());
				} else {
					Notification.show(BUNDLE.getString("login.form.error.caption"), BUNDLE.getString("login.form.error"), Type.ERROR_MESSAGE);
				}
			}
		};
	}
	
	public static Layout createMainForm() {
		VerticalLayout layout = new VerticalLayout();
		TopForm topForm = new TopForm();
		BottomForm bottomForm = new BottomForm();
		layout.addComponent(topForm);
		layout.addComponent(bottomForm);
		layout.setComponentAlignment(topForm, Alignment.MIDDLE_CENTER);
		layout.setComponentAlignment(bottomForm, Alignment.MIDDLE_CENTER);
		return layout;
	}
}
