package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class SendEmailForm extends VerticalLayout {

	private static final long serialVersionUID = 4112595596264323027L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();

	private TextField subjectField;
	private TextArea plotField;
	private ComboBox groupsCbm;

	public SendEmailForm() {
		super();
	}

	@Override
	public void attach() {
		super.attach();
		addComponent(getSubjectField());
		addComponent(getPlotField());
		addComponent(getGroupsCmb());
	}

	public TextField getSubjectField() {
		if (this.subjectField == null) {
			this.subjectField = new TextField();
			this.subjectField.setCaption(BUNDLE
					.getString("send.email.form.subject"));
			this.subjectField.setImmediate(true);
			this.subjectField.setMaxLength(77);
			this.subjectField.setRequired(true);
			this.subjectField.setWidth(300, Unit.PIXELS);
		}
		return subjectField;
	}

	public TextArea getPlotField() {
		if (this.plotField == null) {
			this.plotField = new TextArea();
			this.plotField.setCaption(BUNDLE.getString("send.email.form.plot"));
			this.plotField.setImmediate(true);
			this.plotField.setWidth(300, Unit.PIXELS);
			this.plotField.setHeight(200, Unit.PIXELS);
		}
		return plotField;
	}

	public ComboBox getGroupsCmb() {
		if (this.groupsCbm == null) {
			this.groupsCbm = new ComboBox();
			this.groupsCbm
					.setCaption(BUNDLE.getString("send.email.form.group.caption"));
			this.groupsCbm.setImmediate(true);

			BeanItemContainer<UserGroup> container = new BeanItemContainer<UserGroup>(
					UserGroup.class);

			this.groupsCbm.setContainerDataSource(container);
			this.groupsCbm.setItemCaptionPropertyId("name");
			this.groupsCbm.setNullSelectionAllowed(false);

			container.addAll(GroupDAO.getGroupsEnt());
			
			this.groupsCbm.select(container.getItemIds().iterator().next());
		}
		return this.groupsCbm;
	}
}
