package ua.cn.msender.form;

import ua.cn.msender.component.EmailHistoryTable;
import ua.cn.msender.dao.hibernate.EmailHistoryDAO;
import ua.cn.msender.domain.EmailHistory;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.GridLayout;

public class EmailHistoryTab extends GridLayout {
	private static final long serialVersionUID = 207766208183506970L;
	private EmailHistoryTable emailHistoryTable;

	public EmailHistoryTab() {
		super();
		setRows(1);
		setColumns(1);
		setSizeFull();
	}

	@Override
	public void attach() {
		super.attach();
		addComponent(getEmailHistoryTable(), 0, 0);
	}

	public EmailHistoryTable getEmailHistoryTable() {
		if (this.emailHistoryTable == null) {
			this.emailHistoryTable = new EmailHistoryTable();
			@SuppressWarnings("unchecked")
			BeanItemContainer<EmailHistory> container = (BeanItemContainer<EmailHistory>) this.emailHistoryTable
					.getContainerDataSource();
			container.addAll(EmailHistoryDAO.getEmailHistory());
			this.emailHistoryTable.setSizeFull();
		}
		return emailHistoryTable;
	}
}
