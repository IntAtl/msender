package ua.cn.msender.form;


import ua.cn.msender.component.GroupTable;
import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.domain.UserGroup;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;

public class GroupTab extends GridLayout {
	private static final long serialVersionUID = -4623755876780180587L;
	private GroupTable groupTable;
	
	public GroupTab() {
		super();
		setRows(1);
		setColumns(3);
		setSizeFull();
		setColumnExpandRatio(0, 0.3F);
		setColumnExpandRatio(1, 0.4F);
		setColumnExpandRatio(2, 0.3F);
	}
	
	@Override
	public void attach() {
		super.attach();
		addComponent(getGroupTable(), 1, 0);
		setComponentAlignment(getGroupTable(), Alignment.MIDDLE_CENTER);
	}

	public GroupTable getGroupTable() {
		if(this.groupTable == null) {
			this.groupTable = new GroupTable();
			@SuppressWarnings("unchecked")
			BeanItemContainer<UserGroup> container = (BeanItemContainer<UserGroup>) this.groupTable.getContainerDataSource();
			container.addAll(GroupDAO.getGroupsEnt());
		}
		return groupTable;
	}
}
