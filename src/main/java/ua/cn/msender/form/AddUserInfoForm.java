package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.dao.hibernate.GroupDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class AddUserInfoForm extends VerticalLayout {
	private static final long serialVersionUID = -7478640451146088164L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private static final String FULL_NAME = "fullName";
	private static final String EMAIL = "email";
	private static final String GROUP = "group";

	@PropertyId(FULL_NAME)
	private TextField fullName;
	@PropertyId(EMAIL)
	private TextField email;
	@PropertyId(GROUP)
	private ComboBox group;

	public AddUserInfoForm() {
		super();
	}

	@Override
	public void attach() {
		super.attach();
		addComponent(getFullName());
		addComponent(getEmail());
		addComponent(getGroup());
	}

	public TextField getFullName() {
		if (this.fullName == null) {
			this.fullName = new TextField();
			this.fullName.setCaption(BUNDLE
					.getString("add.worker.form.fullname"));
			this.fullName.setImmediate(true);
			this.fullName.setRequired(true);
			this.fullName.setNullSettingAllowed(false);
			this.fullName.setValidationVisible(true);
			this.fullName
					.addValidator(new StringLengthValidator(BUNDLE
							.getString("add.worker.form.error.fullname"), 1,
							90, false));
		}
		return fullName;
	}

	public TextField getEmail() {
		if (this.email == null) {
			this.email = new TextField();
			this.email.setCaption(BUNDLE.getString("add.worker.form.email"));
			this.email.setImmediate(true);
			this.email.setRequired(true);
			this.email.setNullSettingAllowed(false);
			this.email.setValidationVisible(true);
			this.email.addValidator(new EmailValidator(BUNDLE
					.getString("email.validation.error")));
		}
		return email;
	}

	public ComboBox getGroup() {
		if (this.group == null) {
			this.group = new ComboBox();
			this.group.setCaption(BUNDLE.getString("add.worker.form.group"));
			this.group.setImmediate(true);
			BeanItemContainer<UserGroup> cont = new BeanItemContainer<UserGroup>(
					UserGroup.class, GroupDAO.getGroupsEnt());
			this.group.setContainerDataSource(cont);
			this.group.setItemCaptionPropertyId("name");
			this.group.setRequired(true);
			this.group.setNullSelectionAllowed(false);
			this.group.select(group.getContainerDataSource().getItemIds()
					.iterator().next());
			this.group.setValidationVisible(true);
		}
		return group;
	}
}
