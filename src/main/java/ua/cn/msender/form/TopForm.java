package ua.cn.msender.form;


import java.util.ResourceBundle;

import ua.cn.msender.VaadinSessionAttribute;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

public class TopForm extends GridLayout {
	private static final long serialVersionUID = -6085995798003304164L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private Button logout;
	private Label whois;

	@Override 
	public void attach() {
		super.attach();
		addComponent(getWhoIsLogInLabel(), 0, 0);
		addComponent(getLogout(), 1, 0);
		setComponentAlignment(getWhoIsLogInLabel(), Alignment.MIDDLE_LEFT);
		setComponentAlignment(getLogout(), Alignment.MIDDLE_RIGHT);
	}

	public TopForm() {
		setRows(1);
		setColumns(2);
		setSizeFull();
	}
	
	public Button getLogout() {
		if(this.logout == null) {
			this.logout = new Button();
			this.logout.setCaption(BUNDLE.getString("top.form.logout.button"));
			this.setImmediate(true);
			this.setStyleName(BaseTheme.BUTTON_LINK);
			this.logout.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -2124967781133711966L;
				@Override
				public void buttonClick(ClickEvent event) {
					UI.getCurrent().getSession().setAttribute(VaadinSessionAttribute.USER_ID, null);
					UI.getCurrent().setContent(FormFactory.createLoginForm());
				}
			});
		}
		return logout;
	}

	private Label getWhoIsLogInLabel() {
		if(this.whois == null) {
			this.whois = new Label();
			String caption = BUNDLE.getString("top.form.whois.label") + " " + UI.getCurrent().getSession().getAttribute(VaadinSessionAttribute.LOGIN);
			this.whois.setCaption(caption);
		}
		return this.whois;
	}
}
