package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class AddGroupForm extends VerticalLayout {
	private static final String GROUP_NAME = "name";
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private static final long serialVersionUID = 4639868996170013135L;

	@PropertyId(GROUP_NAME)
	private TextField name;

	public AddGroupForm() {
		super();
	}

	@Override
	public void attach() {
		super.attach();
		addComponent(getName());
	}

	public TextField getName() {
		if (this.name == null) {
			this.name = new TextField();
			this.name.setCaption(BUNDLE.getString("add.group.form.name"));
			this.name.setImmediate(true);
			this.name.setNullRepresentation("");
			this.name.setRequired(true);
			/*
			 * this.name.addValidator(new BeanValidator(UserGroup.class,
			 * GROUP_NAME));
			 */
			this.name.addValidator(new StringLengthValidator(BUNDLE
					.getString("add.group.form.error"), 1, 16, false));
			this.name.setValidationVisible(true);
		}
		return this.name;
	}

}
