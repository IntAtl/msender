package ua.cn.msender.form;

import java.util.ResourceBundle;

import ua.cn.msender.component.AddGroupWindow;
import ua.cn.msender.component.AddUserWindow;
import ua.cn.msender.component.ComponentFactory;
import ua.cn.msender.component.UsersTable;
import ua.cn.msender.dao.hibernate.WorkerDAO;
import ua.cn.msender.domain.Worker;
import ua.cn.msender.util.CaptionBundle;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.VerticalLayout;

public class UsersManagmentTab extends GridLayout {
	private static final long serialVersionUID = 1146992705867931331L;
	private static final ResourceBundle BUNDLE = CaptionBundle.getBundle();
	private Button addUser;
	private Button addGroup;
	private Button sendEmailForGroup;
	private UsersTable usersTable;

	public UsersManagmentTab() {
		super();
		setRows(1);
		setColumns(2);
		setImmediate(true);
		setSizeFull();
		setWidth(100, Unit.PERCENTAGE);
		setColumnExpandRatio(0, 0.9F);
		setColumnExpandRatio(1, 0.1F);
	}

	@Override
	public void attach() {
		super.attach();
		VerticalLayout l = new VerticalLayout();
		l.addComponent(getAddUser());
		l.addComponent(getSendEmailForGroup());
		l.addComponent(getAddGroup());
		l.setWidth(100, Unit.PIXELS);
		addComponent(l, 1, 0);
		addComponent(getUsersTable(), 0, 0);
		setComponentAlignment(getUsersTable(), Alignment.MIDDLE_CENTER);
		l.setComponentAlignment(getAddUser(), Alignment.MIDDLE_LEFT);
		l.setComponentAlignment(getSendEmailForGroup(), Alignment.MIDDLE_LEFT);
	}

	public Button getAddUser() {
		if (this.addUser == null) {
			this.addUser = new Button();
			this.addUser
					.setCaption(BUNDLE
							.getString("bottom.form.users.managment.tab.adduser.button"));
			this.addUser.setImmediate(true);
			this.addUser.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -876822364858333239L;

				@Override
				public void buttonClick(ClickEvent event) {
					@SuppressWarnings("unchecked")
					BeanItemContainer<Worker> container = (BeanItemContainer<Worker>) getUsersTable()
							.getContainerDataSource();
					AddUserWindow w = ComponentFactory
							.createAddWorkerWindow(container);
					w.getAddButton().setVisible(true);
					w.getEditButton().setVisible(false);
					UI.getCurrent().addWindow(w);
					w.center();
				}
			});
		}
		return addUser;
	}

	public Button getAddGroup() {
		if (this.addGroup == null) {
			this.addGroup = new Button();
			this.addGroup
					.setCaption(BUNDLE
							.getString("bottom.form.users.managment.tab.addgroup.button"));
			this.addGroup.setImmediate(true);
			this.addGroup.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					AddGroupWindow w = ComponentFactory.createAddGroupWindow();
					w.getOkBtn().setVisible(true);
					w.getEditBtn().setVisible(false);

					UI.getCurrent().addWindow(w);
					w.center();
				}
			});
		}
		return this.addGroup;
	}

	public Button getSendEmailForGroup() {
		if (this.sendEmailForGroup == null) {
			this.sendEmailForGroup = new Button();
			this.sendEmailForGroup
					.setCaption(BUNDLE
							.getString("bottom.form.users.managment.tab.send.email.for.group"));
			;
			this.sendEmailForGroup.setImmediate(true);
			this.sendEmailForGroup.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -3301450937173286423L;

				@Override
				public void buttonClick(ClickEvent event) {
					Window w = ComponentFactory.createSendEmailToGroupEmailWindow();
					UI.getCurrent().addWindow(w);
					w.center();
				}
			});
		}
		return sendEmailForGroup;
	}

	public Table getUsersTable() {
		if (this.usersTable == null) {
			this.usersTable = new UsersTable();
			this.usersTable.setImmediate(true);
			this.usersTable.setSizeFull();
			this.usersTable.setVisibleColumns(this.usersTable
					.getTableVisibleColumns());
			@SuppressWarnings("unchecked")
			BeanItemContainer<Worker> cont = (BeanItemContainer<Worker>) this.usersTable
					.getContainerDataSource();
			cont.addAll(WorkerDAO.getWorkers());
		}
		return usersTable;
	}
}
