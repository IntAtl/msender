package ua.cn.msender.util;

import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import ua.cn.msender.dao.hibernate.WorkerDAO;
import ua.cn.msender.domain.UserGroup;
import ua.cn.msender.domain.Worker;

public class EmailUtil {
	public static void sendEmailToWorker(String emailTo, String subject,
			String mainBody) {
		try {
			Email email = new SimpleEmail();
			email.addTo(emailTo);
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator(
					"novikvictor@gmail.com", "G_MAIL_PASS"));
			email.setSSLOnConnect(true);
			email.setFrom("novikvictor@gmail.com");
			email.setSubject(subject);
			email.setMsg(mainBody);
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}

	public static void sendEmailToGroup(UserGroup uGroup, String subject,
			String plot) {
		List<Worker> workers = WorkerDAO.getWorkersByGroup(uGroup);
		for (Worker w : workers) {
			sendEmailToWorker(w.getEmail(), subject, plot);
		}
	}
}
