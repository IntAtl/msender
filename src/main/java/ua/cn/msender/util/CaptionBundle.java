package ua.cn.msender.util;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class CaptionBundle {
	private static final String MSENDER_PROPERTIES = "MSenderProperties";
	private static java.util.ResourceBundle RESOURCE_BUNDLE = java.util.ResourceBundle.getBundle(MSENDER_PROPERTIES, new Locale("ru"));
	
	public static String getString(String key) {
		try {
			return new String(RESOURCE_BUNDLE.getString(key).getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static void setLocale(Locale locale) {
		RESOURCE_BUNDLE = java.util.ResourceBundle.getBundle(MSENDER_PROPERTIES, locale);
	}

	public static java.util.ResourceBundle getBundle() {
		return RESOURCE_BUNDLE;
	}
}
