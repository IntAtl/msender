package ua.cn.msender.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;

import ua.cn.msender.util.HibernateUtil;

@WebFilter(value = "/*")
public class HibernateServletFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
		final Session session = HibernateUtil.getSessionFactory()
				.getCurrentSession();

		if (session.getTransaction().isActive()) {
			session.getTransaction().commit();
		}
		if (session.isOpen()) {
			session.close();
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		final Session session = HibernateUtil.getSessionFactory()
				.getCurrentSession();
		try {
			session.beginTransaction();
			chain.doFilter(request, response);
			session.getTransaction().commit();
		} catch (StaleObjectStateException e) {

			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			throw e;
		} catch (Throwable e) {

			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			throw new ServletException(e);
		}
	}

}
