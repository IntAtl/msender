﻿CREATE TABLE worker
(
	"code" serial NOT NULL,
	"full_name" character varying(90) NOT NULL,
	"email" character varying(30) NOT NULL,
	"user_group" integer NOT NULL,
	CONSTRAINT pk_worker PRIMARY KEY("code"),
	CONSTRAINT fk_worker_group FOREIGN KEY("user_group") REFERENCES user_group("code")
);