﻿CREATE TABLE user_group
(
	"code" serial NOT NULL,
	"group_name" character varying(32),
	CONSTRAINT pk_group PRIMARY KEY ("code"),
	CONSTRAINT uniq_group_name UNIQUE ("group_name")
);