﻿CREATE TABLE user_info
(
	"code" serial NOT NULL,
	"user_login" character varying(16) NOT NULL,
	"user_email" character varying(100) NOT NULL,
	"user_password" character varying(16) NOT NULL,
	"user_group" integer NOT NULL,
	CONSTRAINT pk_user PRIMARY KEY("code"),
	CONSTRAINT fk_user_group FOREIGN KEY("code") REFERENCES user_group("code")
);