﻿CREATE TABLE email_history (
	"code" serial NOT NULL,
	"uinfo" integer,
	"worker" integer,
	"ugroup" integer,
	"esubject" character varying(77) NOT NULL,
	"eplot" character varying NOT NULL,
	"edate" timestamp without time zone NOT NULL,
	CONSTRAINT pk_email_history PRIMARY KEY("code"),
	CONSTRAINT fk_email_history_uinfo FOREIGN KEY("uinfo") REFERENCES user_info("code"),
	CONSTRAINT fk_email_history_worker FOREIGN KEY("worker") REFERENCES worker("code"),
	CONSTRAINT fk_email_history_ugroup FOREIGN KEY("ugroup") REFERENCES user_group("code")
);